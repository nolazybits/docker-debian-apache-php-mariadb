FROM nolazybits/debian-apache-php
MAINTAINER Xavier Martin <nolazybits+docker@gmail.com>

ENV DEBIAN_FRONTEND noninteractive

# copy all the scripts to bin
COPY scripts/bin/ /usr/local/bin/
RUN chmod -R +x  /usr/local/bin/*.sh

COPY scripts/config/ /tmp/config/

# install mariaDB and phpmyadmin
ARG MYSQL_ROOT_PASSWORD=root
COPY scripts/01_install_mariadb.sh /tmp/
COPY scripts/02_install_phpmyadmin.sh /tmp/
RUN chmod +x /tmp/01_install_mariadb.sh; sync && \
    chmod +x /tmp/02_install_phpmyadmin.sh; sync && \
    /tmp/01_install_mariadb.sh && \
    /tmp/02_install_phpmyadmin.sh

EXPOSE 3306

CMD ["start.sh"]
